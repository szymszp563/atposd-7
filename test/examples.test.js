const {
  Builder, By, Key, until,
} = require('selenium-webdriver');
const fs = require('fs');
const assert = require('assert');

// test('Google should has Google in title', async () => {
//     // creating a driver
//     const driver = new Builder().forBrowser('chrome').build();
//     // get a page
//     await driver.get('https://www.google.com/');
//     // check if the title contains Google (repeat that process 1 sec. till true)
//     await driver.wait(until.titleContains('Google'), 1000);
//     await driver.quit();
// });
//
// test('Google results should has GitLab link after searching GitLab', async () => {
//     // creating a driver
//     const driver = new Builder().forBrowser('chrome').build();
//     // get a page
//     await driver.get('https://www.google.com/');
//     // send some keys
//     await driver.wait(until.elementLocated(By.name('q')), 1000);
//     await (await driver.findElement({ name: 'q' })).sendKeys('gitlab', Key.ENTER);
//     // check if link is available
//     await driver.wait(until.elementLocated(By.partialLinkText('GitLab.org')), 5000);
//     await driver.quit();
// });

test('Multiplying 6 by 9 should equal 54', async () => {
  const driver = new Builder().forBrowser('chrome').build();
  await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');
  await (await driver.findElement({ name: 'six' })).click();
  await (await driver.findElement({ name: 'mul' })).click();
  await (await driver.findElement({ name: 'nine' })).click();
  await (await driver.findElement({ name: 'result' })).click();
  const result = await (
    await driver.findElement({ name: 'Display' })
  ).getAttribute('value');
  expect(result).toEqual('54');

  driver.takeScreenshot().then((data) => fs.writeFileSync('6mul9.png', data, 'base64'));
  await driver.quit();
});

test('5+5*6 should equal 60', async () => {
  const driver = new Builder().forBrowser('chrome').build();
  await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');
  await (await driver.findElement({ name: 'five' })).click();
  await (await driver.findElement({ name: 'add' })).click();
  await (await driver.findElement({ name: 'five' })).click();
  await (await driver.findElement({ name: 'mul' })).click();
  await (await driver.findElement({ name: 'six' })).click();
  await (await driver.findElement({ name: 'result' })).click();
  const result = await (
    await driver.findElement({ name: 'Display' })
  ).getAttribute('value');
  expect(result).toEqual('60');

  driver.takeScreenshot().then((data) => fs.writeFileSync('5plus5mul6.png', data, 'base64'));
  await driver.quit();
});
