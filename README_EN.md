# Selenium

## Installation

The sample code was written in JavaScript, it requires `node.js` installed to run.
We install the necessary libraries using `npm install`.

The code requires also a `chromedriver`, which can be downloaded from https://chromedriver.chromium.org/ (note! you should download the right version for your Chrome). The program must be available in the environment variable `PATH`.

## Running

Tests can be run with the `npm test` command.

## Task

1. Create a calculator test located at http://www.anaesthetist.com/mnm/javascript/calc.htm.
2. Perform any multiplication and using an assertion check the result.
3. Extend the test by downloading a site screenshot, documentation https://www.selenium.dev/selenium/docs/api/javascript/module/selenium-webdriver/index_exports_WebDriver.html#takeScreenshot.
